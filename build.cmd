set BUILD=%~1
set runuat="C:/Program Files/Epic Games/UE_4.19/Engine/Build/BatchFiles/RunUAT.bat"
set PLUGIN=UnwrapUV

if "%BUILD%"=="" (
  SET BUILD=%~dp0/../../../Artifacts/DigiLabsImporter
)
echo Building into %BUILD%

call %runuat% BuildPlugin -Plugin="%~dp0/%PLUGIN%.uplugin" -Package="%BUILD%"

echo Unstaging files not intended for distribution

rmdir /S/Q "%BUILD%/Saved"
rmdir /S/Q "%BUILD%/Intermediate"
rmdir /S/Q "%BUILD%/Source/%PLUGIN%/Private"
rmdir /S/Q "%BUILD%/Source/%PLUGIN%/Public"